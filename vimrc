set nocompatible

packadd! matchit
packadd! quick-scope

" Just the mouse behaviour. It's default.
behave mswin

" Useful stuff found in vimrc_example.vim.
set backspace=indent,eol,start
set history=50
set number
set ruler
set showcmd
set incsearch

"Theo: "full colors -> gvim-like"
set t_Co=256
"color desert
"color rdark
"color jellybeans
color molokai

" Other useful stuff
set autoindent
set relativenumber
set ignorecase
set smartcase
set shortmess-=S
set cursorline
set nowrap
set shell=/bin/bash\ --rcfile\ ~/.vim/.vimbashrc
set cmdwinheight=17
set list
set listchars=trail:¬,tab:░░,extends:┄,precedes:┄

" Finding files
set path+=**
set wildmenu

" Tab options
set tabstop=4      " real tabs are displayed as 8 spaces
set expandtab      " tabbing while editing inserts spaces. How many?
set softtabstop=4  " ... 4
set shiftwidth=4   " 4 spaces inserted by >>. used by autoindent.

"TODO: Check out what these do.
set encoding=utf-8

"Smooth-Scroll
function! SmoothScroll(direction)

    if ( a:direction == "Down" )
        let s:navkey = "\<C-E>"
    elseif ( a:direction == "Up" )
        let s:navkey = "\<C-Y>"
    elseif ( a:direction == "Left" )
        let s:navkey = "zh"
    elseif ( a:direction == "Right" )
        let s:navkey = "zl"
    else
        return
    endif

    for i in range(1,(&scroll-4)/2)
        exe "normal! 2".s:navkey
        redraw
        sleep 1m
    endfor

    for i in range(1,4,1)
        exe "normal! ".s:navkey
        redraw
        exe "sleep ".i."m"
    endfor

endfunction

map <C-D> :call SmoothScroll("Down")<cr>
map <C-U> :call SmoothScroll("Up")<cr>
map <C-L> :call SmoothScroll("Right")<cr>
map <C-H> :call SmoothScroll("Left")<cr>
map <S-ScrollWheelDown> 6zl
map <S-ScrollWheelUp> 6zh

inoremap <C-BS> <C-w>



" NnoRemaps!
nnoremap <leader>rc     :source $MYVIMRC<CR>
nnoremap <leader>grc    :source $MYGVIMRC<CR>
nnoremap <leader>er     :NERDTreeToggle<CR>
nnoremap <leader>ta     :TagbarToggle<CR>

" Just for safety...
nnoremap ZZ <nop>
nnoremap ZQ <nop>

" Autocommands
au CmdwinEnter [/?]  startinsert

" FORTRAN
"let fortran_fold=1 seems laggy
"set foldmethod=syntax
let fortran_free_source=1
let fortran_do_enddo=1
let fortran_indent_less=1

" As required by fortran parameters. See :help fortran
syntax on

" HASKELL
let hs_highlight_boolean = 1
let hs_highlight_types   = 1
let hs_highlight_more_types = 1
set cpoptions+=M    " Matching parenthesis was a nightmare before this

" hasktags
" copied from "https://github.com/majutsushi/tagbar/wiki#haskell"
"
let g:tagbar_type_haskell = {
    \ 'ctagsbin'  : 'hasktags',
    \ 'ctagsargs' : '-x -c -o-',
    \ 'kinds'     : [
        \  'm:modules:0:1',
        \  'd:data: 0:1',
        \  'd_gadt: data gadt:0:1',
        \  't:type names:0:1',
        \  'nt:new types:0:1',
        \  'c:classes:0:1',
        \  'cons:constructors:1:1',
        \  'c_gadt:constructor gadt:1:1',
        \  'c_a:constructor accessors:1:1',
        \  'ft:function types:1:1',
        \  'fi:function implementations:0:1',
        \  'o:others:0:1'
    \ ],
    \ 'sro'        : '.',
    \ 'kind2scope' : {
        \ 'm' : 'module',
        \ 'c' : 'class',
        \ 'd' : 'data',
        \ 't' : 'type'
    \ },
    \ 'scope2kind' : {
        \ 'module' : 'm',
        \ 'class'  : 'c',
        \ 'data'   : 'd',
        \ 'type'   : 't'
    \ }
\ }

" Latex-Suite
" Note: g:Tex_UseMakefile is by default 1
" To compile an input file that lies deeper in directory hierarchy use
"   touch <main.tex>.latexmain
" where <main.tex> the top-level file that includes.
" latexsuite should work as expected if a Makefile is missing.
"
"filetype plugin indent on          "already set for fortran
filetype plugin indent on
set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"
let g:Tex_DefaultTargetFormat = 'pdf'
let g:Tex_BibtexFlavor = 'biber'
let g:Tex_CompileRule_pdf= 'xelatex -interaction=nonstopmode $*'
let g:Tex_SmartKeyDot=0
let g:Tex_ViewRule_pdf = 'zathura'
let g:Tex_IgnoredWarnings =
    \"Underfull"."\n".
    \"Overfull"."\n".
    \"specifier changed to"."\n".
    \"You have requested"."\n".
    \"Missing number, treated as zero."."\n".
    \"There were undefined references"."\n".
    \"Citation %.%# undefined"."\n".
    \"LaTeX Font Warning: Font shape `%s' undefined"."\n".
    \"LaTeX Font Warning: Some font shapes were not available, defaults substituted."
let g:Tex_IgnoreLevel = 9
let g:Tex_GotoError = 0

" VIm-Airline
set laststatus=2
"let g:airline_theme = 'kolor'
"let g:airline_theme = 'molokai'
let g:airline_theme = 'onedark'
let g:airline_left_sep  = ''
let g:airline_right_sep = ''

" Tagbar
let g:tagbar_soft=1
let g:tagbar_foldlevel=-1
let g:tagbar_left=1
let g:tagbar_compact=1
let g:tagbar_autoshowtag=1
let g:tagbar_autoclose=1
let g:tagbar_autofocus=1

" NERDtree
let NERDTreeShowBookmarks=1

" My surround.vim
let g:surround_{char2nr("#")} = "#if \1Define: \1\n\r\n#endif"

" quick-scope
" Trigger a highlight in the appropriate direction when pressing these keys:
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

