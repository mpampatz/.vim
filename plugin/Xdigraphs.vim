" Extending digraphs for fast code documentation
"Use {char} <BS> {char} to put a digraph and import new ones
"set digraph Not helpfull

"   ⎞
"   ⎟
"   ⎠
digraph (u 9115
digraph (m 9116
digraph (d 9117

"   ⎛
"   ⎜
"   ⎝
digraph )u 9118
digraph )m 9119
digraph )d 9120

"   ⎡
"   ⎢
"   ⎣
digraph [u 9121
digraph [m 9122
digraph [d 9123

"   ⎤
"   ⎥
"   ⎦
digraph ]u 9124
digraph ]m 9125
digraph ]d 9126


"   ⎧
"   ⎨
"   ⎩
"   ⎪
digraph {u 9127
digraph {m 9128
digraph {d 9129
digraph {v 9130

"   ⎫
"   ⎬
"   ⎭
"   ⎮
digraph }u 9131
digraph }m 9132
digraph }d 9133
digraph }v 9134

" Small latin subscripts
" Available set { ₐ ₑ ₕ ᵢ ⱼ ₖ ₗ ₘ ₙ ₒ ₚ ᵣ ₛ ₜ ᵤ ᵥ ₓ }
"
"   ₐ
digraph _a 8336
"   ₑ
digraph _e 8337
"   ₕ
digraph _h 8341
"   ᵢ
digraph _i 7522
"   ⱼ
digraph _j 11388
"   ₖ
digraph _k 8342
"   ₗ
digraph _l 8343
"   ₘ
digraph _m 8344
"   ₙ
digraph _n 8345
"   ₒ
digraph _o 8338
"   ₚ
digraph _p 8346
"   ᵣ
digraph _r 7523
"   ₛ
digraph _s 8347
"   ₜ
digraph _t 8348
"   ᵤ
digraph _u 7524
"   ᵥ
digraph _v 7525
"   ₓ
digraph _x 8339


" Small latin superscripts
" Available set: ᵃᵇᶜᵈᵉᶠᵍʰⁱʲᵏˡᵐⁿᵒᵖʳˢᵗᵘᵛʷˣʸᶻ
"

"   ᵃ
digraph ^a 7491
"   ᵇ
digraph ^b 7495
"   ᶜ
digraph ^c 7580
"   ᵈ
digraph ^d 7496
"   ᵉ
digraph ^e 7497
"   ᶠ
digraph ^f 7584
"   ᵍ
digraph ^g 7501
"   ʰ
digraph ^h 688
"   ⁱ
digraph ^i 8305
"   ʲ
digraph ^j 690
"   ᵏ
digraph ^k 7503
"   ˡ
digraph ^l 737
"   ᵐ
digraph ^m 7504
"   ⁿ
digraph ^n 8319
"   ᵒ
digraph ^o 7506
"   ᵖ
digraph ^p 7510
"   ʳ
digraph ^r 691
"   ˢ
digraph ^s 738
"   ᵗ
digraph ^t 7511
"   ᵘ
digraph ^u 7512
"   ᵛ
digraph ^v 7515
"   ʷ
digraph ^w 695
"   ˣ
digraph ^x 739
"   ʸ
digraph ^y 696
"   ᶻ
digraph ^z 7611

" Small numbers superscripts + etc
" Available set: ⁰ ¹ ² ³ ⁴ ⁵ ⁶ ⁷ ⁸ ⁹ ⁺ ⁻ ⁼ ⁽ ⁾

"   ⁰
digraph ^0 8304
"   ¹
digraph ^1 185
"   ²
digraph ^2 178
"   ³
digraph ^3 179
"   ⁴
digraph ^4 8308
"   ⁵
digraph ^5 8309
"   ⁶
digraph ^6 8310
"   ⁷
digraph ^7 8311
"   ⁸
digraph ^8 8312
"   ⁹
digraph ^9 8313
"   ⁺
digraph ^+ 8314
"   ⁻
digraph ^- 8315
"   ⁼
digraph ^= 8316
"   ⁽
digraph ^( 8317
"   ⁾
digraph ^) 8318


" Small numbers superscripts + etc
" Available set: ₀ ₁ ₂ ₃ ₄ ₅ ₆ ₇ ₈ ₉ ₊ ₋ ₌ ₍ ₎

"   ₀
digraph _0 8320
"   ₁
digraph _1 8321
"   ₂
digraph _2 8322
"   ₃
digraph _3 8323
"   ₄
digraph _4 8324
"   ₅
digraph _5 8325
"   ₆
digraph _6 8326
"   ₇
digraph _7 8327
"   ₈
digraph _8 8328
"   ₉
digraph _9 8329
"   ₊
digraph _+ 8330
"   ₋
digraph _- 8331
"   ₌
digraph _= 8332
"   ₍
digraph _( 8333
"   ₎
digraph _) 8334


"TODO:
"ᴬ ᴮ ᴰ ᴱ ᴳ ᴴ ᴵ ᴶ ᴷ ᴸ ᴹ ᴺ ᴼ ᴾ ᴿ ᵀ ᵁ ⱽ ᵂ
"ᵅ ᵝ ᵞ ᵟ ᵋ ᶿ ᶥ ᶲ ᵠ ᵡ ᵦ ᵧ ᵨ ᵩ ᵪ
"
"

"   "   ᵅ
"   digraph ga 7493
"   "   ᵝ     
"   digraph gb 7517
"   "   ᵞ     
"   digraph gg 7518
"   "   ᵟ     
"   digraph gd 7519
"   "   ᵋ     
"   digraph ge 7499
"   "   ᶿ     
"   digraph gt 7615
"   "   ᶥ     
"   digraph gi 7589
"   "   ᶲ     
"   "digraph gp 7602
"   "   ᵠ     
"   digraph gp 7520
"   "   ᵡ     
"   digraph gc 7521
"   "   ᵦ     
"   digraph g  7526
"   "   ᵧ     
"   digraph g  7527
"   "   ᵨ     
"   digraph g  7528
"   "   ᵩ     
"   digraph g  7529
"   "   ᵪ     
"   digraph g  7530
"   
