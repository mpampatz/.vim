" Window positioning
" set lines=55 columns=113
" winpos 0 0 

" FONTS
"set guifont=Monospace\ 11
set guifont=Source\ Code\ Pro\ Medium\ 11
"set guifont=Monaco\ for\ Powerline\ 11

" COLORSCHEMES
"color wolfpack
"color jellybeans
"color rdark

"let g:solarized_contrast="high"    "default value is normal
"let g:solarized_visibility="high"    "default value is normal
"syntax enable
"set background=dark
"colorscheme solarized

colorscheme molokai


" Remove toolbar and scrolbars
set guioptions-=T
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L
set guioptions-=m "use se go+=m to toggle (+/-)
set guioptions-=e "use se go+=m to toggle (+/-)

" Hide mouse when typing text
set mousehide


"set guiheadroom=0
