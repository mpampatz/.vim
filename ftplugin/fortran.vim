" It defines some usefull debugging commands and makes Airline display
" current subroutine name.
" 
" Note: Requires 'set ignorecase'. Also requires Airline but only for
" displaying current subroutine.


"-------------start of declarations-------------------

function! MyFortran_DebugComment()
    let s:line = getline(".")
    call setline(line("."), s:cout_str.s:line)
endfunction



function! MyFortran_DebugInsert()
    let s:cin_list = [s:cin_str.' v-v-v-v-v-v-v-v-v-v-v',
                \     s:cin_str.' ^-^-^-^-^-^-^-^-^-^-^']
    call append(line("."), s:cin_list)
    call cursor(line(".")+1, 0)
    normal! o    
    startinsert!
endfunction



function! MyFortran_GetCurrentSubroutineStats()
    let s:subr_end_ln   = search('^\s*\<end\>', 'nW', 0)
    if s:subr_end_ln == 0
        return ['nosubroutine',0,0]
    endif
    let s:subr_next_ln = search('^\s*\<subroutine\>', 'nW', 0)
    if s:subr_next_ln != 0
        if s:subr_next_ln < s:subr_end_ln
            return ['nosubroutine',0,0]
        endif
    endif
    let s:subr_start_ln = search('^\s*\<subroutine\>', 'nbW', 0)
    if s:subr_start_ln == 0
        return ['nosubroutine',0,0]
    endif
    let s:subr_name = MyFortran_GetSubroutineName(s:subr_start_ln)
    return [s:subr_name, s:subr_start_ln, s:subr_end_ln]
endfunction



function! MyFortran_GetSubroutineName(ln)
    let s:line = getline(a:ln)
    let s:line = substitute(s:line,'^\s*\<subroutine\>\s*','','')
    let s:line = substitute(s:line,'\s*(.*$','','')
    return s:line
endfunction



function! MyFortran_GetCurrentSubroutineCalls()
    let s:save_curpos = getpos(".")

    let s:cs_stats = MyFortran_GetCurrentSubroutineStats()
    if s:cs_stats[0] == 'nosubroutine'
        echoe 'FortranDebugScripts:  Not inside any subroutine'
        return
    endif

    let s:calls_list_lns = []
    call cursor(s:cs_stats[1],0)
    while search("^[ ]*call[ ]*", 'W', s:cs_stats[2])
        call add(s:calls_list_lns, line("."))
    endwhile

    call setpos(".", s:save_curpos)

    return s:calls_list_lns
endfunction



function! MyFortran_GetSubroutineCalled()
    let s:line = getline(".")
    let s:line = substitute(s:line, '^\s*\<call\>\s*','','')
    let s:line = substitute(s:line,'\s*(.*$','','')
    return s:line
endfunction

function! MyFortran_EnterVisualPair(start,middle,end)
    " move cursor to start and save position
    " this way initial position is put in jumplist. 
    " use ctrl-o restore curpos after exiting visualline mode.
    let s:posstart = searchpairpos(a:start,a:middle,a:end,'sb')
    if s:posstart == [0,0] 
        echo "Searched pair not found"
        return
    endif

    " don't move cursor to end. just calculate deltapos
    let s:posend   = searchpairpos(a:start,a:middle,a:end,'n')
    let s:deltapos = s:posend[0] - s:posstart[0]

    " now move visually to end 
    execute "normal!" . "1jV" . s:deltapos . "jkk"
endfunction
"-------------end of declarations-------------------

"------- configuration -------------
let s:cout_str = '!--basil_debug--'
let s:cin_str =  '!++basil_debug++'
let s:fwrite = 'write(*,*) '
"------- end of configuration ------


"OLD: using the customs above
""Note: Not sure if it's the best way to do this but...
"let g:airline_section_b = '%{MyFortran_GetCurrentSubroutineStats()[0]}'
"AirlineRefresh

"NEW: using tagbar. tagbar's great!
"let g:airline_section_b = "%{tagbar#currenttag('%s','notag')}"
"AirlineRefresh

noremap <leader>do :call MyFortran_DebugComment()
noremap <leader>di :call MyFortran_DebugInsert()
"noremap <leader>dw :call MyFortran_WriteCall()
"noremap <leader>de :call MyFortran_WriteEveryCall()

"Compile % run  ---------------------------------------------------------------
"Old and without quickfix
"noremap \wc :w:!   gfortran -std=f2008 -pedantic-errors -fmax-errors=1 -Wall %
"noremap \wr :w:!if gfortran -std=f2008 -pedantic-errors -fmax-errors=1 -Wall %; then ./a.out; fi
"

"New compile configuration. User doesn't explicitly call shell with ":!" or
" ":sh" but using vim's internal ":make". See ":help quickfix.txt".
set makeprg=gfortran\ %\ $*
set errorformat=
            \%A%f:%l:%c:,
            \%-C\ %#1,
            \%-C\ %.%#,
            \%Z%trror:\ %m\,    "old:\%Z%trror:\ %m\ at%.%#,
            \%Z%tarning:\ %m\,  "old:\%Z%tarning:\ %m\ at%.%#,
            \%-C

nnoremap \co :w\|make -Wall -std=f2008 -pedantic-errors \|cw
nnoremap \ch :w\|make -fsyntax-only -Wall -std=f2008 -pedantic-errors \|cw
nnoremap \cp :w\|make -Ofast      \|cw
nnoremap \cd :w\|make -Wall -fbacktrace -fcheck=all      -std=f2008 -pedantic-errors \|cw
"nnoremap \co :w\|make -Wall \|cw
""fortran77
"nnoremap \co :w\|silent make \| cw
"
nnoremap \ma :w\|!make

nnoremap \ru :!./a.out

"assembly code
nnoremap \as :w\|!gfortran -S %
"end Compile % run  ------------------------------------------------------------


"search & jump to nested flow controler ----------------------------------------
nnoremap \/d m':call searchpair('^\s*\<do\>','','^\s*\<end\s*do\>')
nnoremap \?d m':call searchpair('^\s*\<do\>','','^\s*\<end\s*do\>','b')
nnoremap \vd :call MyFortran_EnterVisualPair('^\s*\<do\>','','^\s*\<end\s*do\>')

nnoremap \/s m':call searchpair('^\s*\<subroutine\>','','^\s*\<end\>')
nnoremap \?s m':call searchpair('^\s*\<subroutine\>','','^\s*\<end\>','b')
nnoremap \vs :call MyFortran_EnterVisualPair('^\s*\<subroutine\>','','^\s*\<end\>')

nnoremap \/i m':call searchpair('^\s*\<if\>.*\<then\>','','^\s*\<end\s*if\>')
nnoremap \?i m':call searchpair('^\s*\<if\>.*\<then\>','','^\s*\<end\s*if\>','b')
nnoremap \vi :call MyFortran_EnterVisualPair('^\s*\<if\>.*\<then\>','','^\s*\<end\s*if\>')


"search & jump to nested flow controler ----------------------------------------

"using /usr/share/vim/vimfiles/plugin/imaps.vim installed by vim-latexsuite.
"remember use <CTRL-J> to "JumpForward".
"Since imaps.vim's path is included in "runtimepath" option, it will be loaded 
"on startup so no worries.
call IMAP("if "         , "if ( <++> ) then\<cr><++>\<cr>else\<cr><++>\<cr>endif !<++>"   , "fortran")
call IMAP("do "         , "do <++>\<cr><++>\<cr>enddo !<++>"                              , "fortran")
call IMAP("program "    , "program <++>\<cr>implicit none\<cr><++>\<cr>end program"       , "fortran")
call IMAP("subroutine " , "subroutine <++>\<cr>implicit none\<cr><++>\<cr>end subroutine" , "fortran")
call IMAP("function "   , "function <++>\<cr>implicit none\<cr><++>\<cr>end function"     , "fortran")
"call IMAP("pure "       , "pure function <++>\<cr><++>\<cr>end function"                  , "fortran")
call IMAP("module "     , "module <++>\<cr><++>\<cr>end module"                           , "fortran")
call IMAP("type "       , "type <++>\<cr><++>\<cr>end type"                               , "fortran")
call IMAP("open("       , "open( <++>, file='<++>', action='<++>' )"                      , "fortran")
call IMAP("close("      , "close(<++>)"                                                   , "fortran")
call IMAP("write("      , "write(<++>,<++>) <++>"                                         , "fortran")
call IMAP("read("       , "read(<++>,<++>) <++>"                                          , "fortran")
"call IMAP("dimension(", "dimension(<++>) :: <++>"                                         , "fortran")
"call IMAP("(", "(<++>) <++>"                                                       , "fortran")
call IMAP("allocatable,", "allocatable, dimension(<++>) :: <++>"                          , "fortran")


"set the wordlist for autocompletion
set cpt+=k$HOME/.vim/lib/fortran/fortran.txt

"set colorcolumn=72  I set im vimrc cc=80. Thats OK in modern fortran

" automatically save and restore views for *.f90 files:
au BufWinLeave *.f90 mkview
au BufWinEnter *.f90 silent loadview
