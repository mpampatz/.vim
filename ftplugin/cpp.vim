
nnoremap \ru :!./%:r
" nnoremap \co :w:!make %:r
nnoremap \co :w:!g++ -ansi -pedantic -Wall -o %:r %




"search & jump to nested flow controler ----------------------------------------

"using /usr/share/vim/vimfiles/plugin/imaps.vim installed by vim-latexsuite.
"remember use <CTRL-J> to "JumpForward".
"Since imaps.vim's path is included in "runtimepath" option, it will be loaded 
"on startup so no worries.
"
call IMAP("#in"         , "#include <<++>>"   , "cpp")

call IMAP("if "         , "if (<++>) {\<cr><++>\<cr>} else {\<cr><++>\<cr>}"   , "cpp")
call IMAP("for "         , "for (<++>;<++>;<++>) {\<cr><++>\<cr>}"   , "cpp")
call IMAP("while "         , "while (<++>) {\<cr><++>\<cr>}"   , "cpp")

call IMAP("int main "         , "int main(int argc, char *argv[])\<cr>{\<cr><++>;\<cr>return 0;\<cr>}"   , "cpp")
call IMAP("sizeof"         , "sizeof(<++>)<++>"   , "cpp")

call IMAP("printf"         , "printf(\"<++>\",<++>);"   , "cpp")
call IMAP("scanf"         , "scanf(\"<++>\",<++>);"   , "cpp")

call IMAP("malloc"         , "malloc(<+ size_t size +>);"   , "cpp")
call IMAP("memcpy"         , "memcpy(<+ void *dest +>,<+ const void *src +>, <+ size_t n +>);"   , "cpp")
call IMAP("atoi"         , "atoi(<+const char *npt+>);"   , "cpp")

