
"Vim sources this for both c and cpp filetypes. Dont want that.
if (expand("%:e") == "cpp")
    finish
endif

nnoremap \ru :!./%:r
" nnoremap \co :w:!make %:r
nnoremap \co :w:!gcc -lm % -o %:r
nnoremap \cm :w:make




"search & jump to nested flow controler ----------------------------------------

"using /usr/share/vim/vimfiles/plugin/imaps.vim installed by vim-latexsuite.
"remember use <CTRL-J> to "JumpForward".
"Since imaps.vim's path is included in "runtimepath" option, it will be loaded 
"on startup so no worries.
"
call IMAP("#in"         , "#include <<++>>"   , "c")

call IMAP("if "         , "if (<++>) {\<cr><++>\<cr>} else {\<cr><++>\<cr>}"   , "c")
call IMAP("for "        , "for (<++>;<++>;<++>) {\<cr><++>\<cr>}"   , "c")
call IMAP("while "      , "while (<++>) {\<cr><++>\<cr>}"   , "c")

call IMAP("int main "   , "int main(int argc, char *argv[])\<cr>{\<cr><++>;\<cr>return 0;\<cr>}"   , "c")
call IMAP("sizeof"      , "sizeof(<++>)<++>"   , "c")

"   stdlib.h
call IMAP("malloc"      , "malloc(<+ size_t size +>);"   , "c")
call IMAP("calloc"      , "calloc(<+ size_t nmemb +>, <+ size_t size +>);"   , "c")
call IMAP("free"        , "free(<+ void *ptr +>)", "c")
call IMAP("realloc"     , "realloc(<+ void *ptr +>, <+ size_t size +>)", "c")

call IMAP("atoi"        , "atoi(<+ const char *nptr +>)", "c")
call IMAP("atol"        , "atol(<+ const char *nptr +>)", "c")
call IMAP("atof"        , "atof(<+ const char *nptr +>)", "c")

"   stdio.h
call IMAP("printf"      , "printf(\"<++>\", <++>);"   , "c")
call IMAP("scanf"       , "scanf(\"<++>\", <++>);"   , "c")
call IMAP("fscanf"      , "fscanf(<+ FILE *stream +>, \"<++>\", <++>);"   , "c")
call IMAP("sscanf"      , "sscanf(<+ const char *str +>, \"<++>\", <++>);"   , "c")
call IMAP("getline"     , "getline(<+ char **lineptr +>, <+ size_t *n +>, <+ FILE *stream +>)", "c")

call IMAP("fopen"       , "fopen(<+ const char *pathname +>, <+ const char *mode +>)", "c")
call IMAP("fclose"      , "fclose(<+ FILE *stream +>)", "c")

"   string.h
call IMAP("memcpy"      , "memcpy(<+ void *dest +>, <+ const void *src +>, <+ size_t n +>)"   , "c")
call IMAP("memmove"     , "memmove(<+ void *dest +>, <+ const void *str +>, <+ size_t n +>)" , "c")
call IMAP("strcpy"      , "strcpy(<+ char *dest +>, <+ const char *src +>)", "c")
call IMAP("strncpy"     , "strncpy(<+ char *dest +>, <+ const char *src +>, <+ size_t n +>)", "c")
call IMAP("strdub"      , "strdub(<+ const char *s +>)", "c")

call IMAP("strtok"      , "strtok(<+ char *str +>, <+ const char *delim +>)", "c")

